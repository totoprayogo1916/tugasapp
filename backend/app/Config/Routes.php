<?php

use App\Controllers\User;
use CodeIgniter\Router\RouteCollection;

/**
 * @var RouteCollection $routes
 */
$routes->get('/', 'Home::index');

$routes->resource('tugas');

$routes->post('login', [User::class, 'login']);
$routes->post('register', [User::class, 'register']);
