<?php

namespace App\Controllers;

use App\Entities\User as EntitiesUser;
use CodeIgniter\RESTful\ResourceController;
use Config\Services;

class User extends ResourceController
{
    protected $modelName = 'App\Models\User';
    // protected $format    = 'json';

    /**
     * Return an array of resource objects, themselves in array format
     *
     * @return mixed
     */
    public function login()
    {
        return $this->respond($this->request->getPost());
    }
    
    function register()
    {
        $response = Services::response();
        
        $response->setHeader('Access-Control-Allow-Origin', '*');

        $data = $this->request->getPost();

        $user = new EntitiesUser();
        $user->fill($data);

        $this->model->save($user);

        return $this->respond($this->request->getPost());
    }
}
