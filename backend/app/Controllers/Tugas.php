<?php

namespace App\Controllers;

use CodeIgniter\RESTful\ResourceController;
use Config\Services;

class Tugas extends ResourceController
{
    protected $modelName = 'App\Models\Tugas';
    protected $format    = 'json';

    /**
     * Return an array of resource objects, themselves in array format
     *
     * @return mixed
     */
    public function index()
    {
        $getData = $this->model->findAll();

        $data = [
            'data' => $getData,
            'status' => [ 
                'code' => Services::response()->getStatusCode(),
                'description' => empty($getData) ? 'Data empty' : 'Data found'
            ]
        ];

        return $this->respond($data);
    }

    /**
     * Return the properties of a resource object
     *
     * @return mixed
     */
    public function show($id = null)
    {
        $getData = $this->model->find($id) ?? [];

        $data = [
            'data' => $getData,
            'status' => [ 
                'code' => Services::response()->getStatusCode(),
                'description' => $getData === [] ? 'Data not found' : 'Data found'
            ]
        ];

        return $this->respond($data);
    }

    /**
     * Create a new resource object, from "posted" parameters
     *
     * @return mixed
     */
    public function create()
    {
        $id = $this->model->insert($this->request->getPost());
        $affected = db_connect()->affectedRows();

        $data = [
            'data' => $this->model->find($id),
            'status' => [ 
                'code' => Services::response()->getStatusCode(),
                'description' => $affected > 0 ? 'Data inserted successfully' : 'Data inserted failed'
            ]
        ];

        return $this->respond($data);
    }

    /**
     * Add or update a model resource, from "posted" properties
     *
     * @return mixed
     */
    public function update($id = null)
    {
        $this->model->update($id, $this->request->getRawInput());
        $affected = db_connect()->affectedRows();

        $data = [
            'data' => $this->model->find($id),
            'status' => [ 
                'code' => Services::response()->getStatusCode(),
                'description' => $affected > 0? 'Data updated successfully' : 'Data updated failed'
            ]
        ];

        return $this->respond($data);
    }

    /**
     * Delete the designated resource object from the model
     *
     * @return mixed
     */
    public function delete($id = null)
    {
        $this->model->delete($id);

        $affected = db_connect()->affectedRows();

        $data = [
            'status' => [ 
                'code' => Services::response()->getStatusCode(),
                'description' => $affected > -1 ? 'Data Deleted': 'Delete Failed',
            ]
        ];

        return $this->respond($data);
    }
}
